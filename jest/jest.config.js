const path = require('path');

console.log(path.join(__dirname, '../src/redux'))

module.exports = {
	rootDir: path.join(__dirname, '../src'),
	moduleNameMapper: {
		'^\$libs': path.join(__dirname, '../src/libs'),
		'^\$layout': path.join(__dirname, '../src/layout'),
		'^\$components': path.join(__dirname, '../src/components'),
		'^\$redux': path.join(__dirname, '../src/redux'),
	},
};