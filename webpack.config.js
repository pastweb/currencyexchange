const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const assetManager = require('node-sass-asset-manager');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const sass_loader_options = {
	functions: assetManager({
    	images: {
    		src_root: 'src/assets/imgs/',
    		dest_root: path.resolve(__dirname, './dist/'),
    		path_prefix: '/assets/imgs/',
    	},
    	fonts: {
    		src_root: 'src/assets/',
    		dest_root: path.resolve(__dirname, './dist/'),
    		path_prefix: '/assets/',
    	},
	}),
  	data: '@import "./src/style/variables"; @import "./src/style/mixins";',
};

const css_loader_options = {
	sourceMap: true,
	camelCase: true,
	getLocalIdent: (context, localIdentName, localName) => {
	  	const componentName = context.resourcePath.split('/').slice(-2, -1);
	  	return `${componentName}_${localName}`;
	},
	importLoaders: 2,
	minimize: false,
	modules: true,
};

const devServerConfig = {
	host: 'localhost',
	port: 8080,
};

module.exports = {
	entry: [
	    // Set up an ES6-ish environment
	    '@babel/polyfill',
	    // app entry point
	    './src/index.js',

	    `webpack-dev-server/client?http://${devServerConfig.host}:${devServerConfig.port}`,
	    // bundle the client for webpack-dev-server
	    // and connect to the provided endpoint

	    'webpack/hot/only-dev-server',
	    // bundle the client for hot reloading
	    // only- means to only hot reload for successful updates
	],

	output: {
	    filename: '[name].js',
	    chunkFilename: '[name]-[chunkhash].chunk.js',
	    path: path.resolve(__dirname, './dist/'),
	    publicPath: `http://${devServerConfig.host}:${devServerConfig.port}/`,
	},

	devtool: 'source-map',

	devServer: {
		host: devServerConfig.host,
		port: devServerConfig.port,

		headers: {
		  'Access-Control-Allow-Origin': '*',
		},
		hot: true,
		// enable HMR on the server

		contentBase: path.resolve(__dirname, './dist'),

		// match the output `publicPath`
		publicPath: `http://${devServerConfig.host}:${devServerConfig.port}/`,

		// Alway Serve the index.html unless the path matches another asset e.g js/css/png... ect
		historyApiFallback: true,
	},

	optimization: {
		runtimeChunk: true,
    	splitChunks: {
      	// include all types of chunks
      	chunks: 'all',
    	},
	},

	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			$libs: path.join(__dirname, './src/libs'),
			$layout: path.join(__dirname, './src/layout'),
			$components: path.join(__dirname, './src/components'),
			$redux: path.join(__dirname, './src/redux'),
		},
	},

	plugins: [
  		new CleanWebpackPlugin(['dist'], { root: path.resolve(__dirname, './') }),
		new webpack.HotModuleReplacementPlugin(),
		// enable HMR globally

		new webpack.NamedModulesPlugin(),
		// prints more readable module names in the browser console on HMR updates,
		new webpack.optimize.ModuleConcatenationPlugin(),
		new HtmlWebpackPlugin({ template: './src/index.html', }),
	],

	module: {
		rules: [
			{
				test: /\.js|.jsx?$/,
		        exclude: [/node_modules/],
		        use: ['babel-loader'],
			},
			{
		        test: /\.(css|sass|scss)?$/,
		        exclude: [/node_modules|\.m\.(css|sass|scss)?$/],
		        use: [
		        	{ loader: 'style-loader', options: { sourceMap: true } },
		        	{ loader: 'css-loader' },
		        	{ loader: 'postcss-loader',	options: { plugins: () => [autoprefixer()] } },
		          	{ loader: 'sass-loader', options: sass_loader_options },
		        ],
		    },
			{
		        test: /\.m\.(css|sass|scss)?$/,
		        exclude: [/node_modules/],
		        use: [
		        	{ loader: 'style-loader', options: { sourceMap: true } },
		        	{ loader: 'css-loader', options: css_loader_options },
		        	{ loader: 'postcss-loader',	options: { plugins: () => [autoprefixer()] } },
		          	{ loader: 'sass-loader', options: sass_loader_options },
		        ],
		    },
		],
	},
};