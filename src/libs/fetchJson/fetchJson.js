import axios from 'axios';
import debug from 'debug';

const log = debug('app:fetchJson');

/*
*
*/

const defaultOptions = {
  headers: {
    'Content-type': 'application/json',
  },
  method: 'GET',
};

/*
*
*/

const requestInterceptor = function (config) {
  log('Sending %s request to %s', config.method, config.url);
  log({ config }, 'Request config');
  return config;
};

/*
*
*/

const successResponseInterceptor = function (res) {
  log('Recieved %s response from upstream %s %s',
    res.status, res.config.method, res.config.url);
  log({ res }, 'Response');

  return res;
};

/*
*
*/

const errorResponseInterceptor = function (err) {
  log({ err }, 'Recieved err response from upstream');

  throw err;
};

/*
*
*/

function FetchJson(config) {
  const agent = axios.create(Object.assign(defaultOptions, config));
  agent.interceptors.request.use(requestInterceptor);
  agent.interceptors.response.use(successResponseInterceptor, errorResponseInterceptor);
  return agent;
}

export default FetchJson;
