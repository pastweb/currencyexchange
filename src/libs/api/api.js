import debug from 'debug';
import FetchJson from '$libs/fetchJson/fetchJson';

const log = debug('app:libs:api');

/**
* Error Key e.g Not Found to notFound
**/

const getErrorKey = errorCode => 
  `${errorCode.charAt(0).toLowerCase()}${errorCode.slice(1)}`.replace(/\s/, '');

/**
* Reusable request agent
* No need to apply configuration each time
* e.g api.get('/customers')
**/

const api = new FetchJson({
  baseURL: 'https://openexchangerates.org/api',
});

export function errorsResponseInterceptor(error) {
  log('Transforming error response from', error.response, error);

  // Oops... there was an unknown error. E.g Network Error
  if (!error || !error.response) {
    log('No network');
    Object.assign(error, {
      response: {
        data: {
          network: {
            message: 'Network Error',
            code: 'network',
            statusCode: 0,
          },
        },
      },
    });
  } else if (error.response.data && error.response.data.code) {
    log('Normalise HttpErrorResponse');
    // convert 'NotFound' to notFound
    const key = getErrorKey(error.response.data.code);
    Object.assign(error, {
      response: {
        data: {
          [key]: error.response.data,
        },
      },
    });
  }
  log('Transforming error response to', error.response);

  throw error;
}

api.interceptors.response.use(null, errorsResponseInterceptor);

export default api;
