export function getFloats(number){
	let result = number.toString();
	const splitted = result.split('.');
	if(splitted[1] && (splitted[1].length > 2)){
		result = parseFloat(result).toFixed(2);
	}
	return result; 
}