import React, { Component } from 'react';
import PropTypes from 'prop-types';;
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as curreActions from '$redux/currencies/currActions';
import { getFloats } from '$libs/util';
import ArrowBox from '$components/arrowBox/arrowBox';
import ScreenSlider from '$components/screenSlider/screenSlider';
import style from './upScreen.m.scss';

const mapStateToProps = state => ({
  upCurr: state.currencies.upScreen,
  downCurr: state.currencies.downScreen,
  rates: state.currencies.rates,
  amount: state.currencies.amount,
});

const mapDispatchToProps = dispatch => ({
  actions: {
    setScreenCurrency: bindActionCreators(curreActions.setScreenCurrency, dispatch),
    setAmount: bindActionCreators(curreActions.setAmount, dispatch),
  },
});

const symbols = {
	USD: '$',
	GBP: '£',
	EUR: '€',
};

class UpScreen extends Component {
	static propTypes = {
		upCurr: PropTypes.string,
		downCurr: PropTypes.string,
		actions: PropTypes.object,
	};
	static defaultProps = {
		upCurr: 'USD',
		downCurr: 'USD',
		actions: {
			setScreenCurrency: () => {},
			setAmount: () => {},
		},
	};
	constructor(props){
		super(props);

		this.state = {
			width: window.innerWidth,
			height: window.innerHeight / 2,
		};

		this.currSymbol = '$';
		this.altSymbol = null;
		this.altValue = null;
		this.onResize = this.onResize.bind(this);
		this.onAmountChange = this.onAmountChange.bind(this);
		this.onCurrencyChange = this.onCurrencyChange.bind(this);
	}

	componentDidMount(){
		window.addEventListener('resize', this.onResize);
		window.addEventListener('orientationchange', this.onResize);
	}

	componentWillReceiveProps(newProps){
		const { upCurr, downCurr, rates } = newProps;
		if(Object.keys(rates).length > 0){
			this.currSymbol = symbols[upCurr];
			this.altSymbol = symbols[downCurr];
			this.altValue = getFloats((1 * rates[upCurr]) / rates[downCurr]);
		}
	}

	componentWillUnmount(){
		window.removeEventListener('resize', this.onResize);
		window.removeEventListener('orientationchange', this.onResize);
	}

	onResize(){
		this.setState({
			width: window.innerWidth,
			height: window.innerHeight / 2,
		});
	}

	onAmountChange(value){
		this.props.actions.setAmount(value.toString());
	}

	onCurrencyChange(base){
		const { setScreenCurrency } = this.props.actions;
		setScreenCurrency(base, 'upScreen');
	}

	render(){
		const { width, height } = this.state;
		return(
			<div className={style.upScreen} style={{ height }}>
				<ArrowBox arrowPosition="bottom"
						  arrowSize={10}
						  bgColor="#2569FA"
						  borderColor="#2569FA"
						  textColor="white"
						  boxShadow="0px 0px 0px 0px #FFFFFF">
					<div style={{ width, height }}>
						<ScreenSlider height={height}
									  value={this.props.amount}
									  autoFocus={true}
									  dispCurr={this.currSymbol}
									  dispAltCurr={this.altSymbol}
									  dispAltValue={this.altValue}
									  onAmountChange={this.onAmountChange}
									  onCurrencyChange={this.onCurrencyChange} />
					</div>
				</ArrowBox>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UpScreen);
