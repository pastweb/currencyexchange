import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as curreActions from '$redux/currencies/currActions';
import { getFloats } from '$libs/util';
import ScreenSlider from '$components/screenSlider/screenSlider';
import style from './downScreen.m.scss';

const mapStateToProps = state => ({
  upCurr: state.currencies.upScreen,
  downCurr: state.currencies.downScreen,
  rates: state.currencies.rates,
  amount: state.currencies.amount,
});

const mapDispatchToProps = dispatch => ({
  actions: {
    setScreenCurrency: bindActionCreators(curreActions.setScreenCurrency, dispatch),
  },
});

const symbols = {
	USD: '$',
	GBP: '£',
	EUR: '€',
};

class DownScreen extends Component {
	static propTypes = {
		upCurr: PropTypes.string,
		downCurr: PropTypes.string,
		actions: PropTypes.object,
		amount: PropTypes.string,
	};
	static defaultProps = {
		upCurr: 'USD',
		downCurr: 'USD',
		actions: {
			setScreenCurrency: () => {},
		},
		amount: '',
	};
	constructor(props){
		super(props);

		this.state = {
			width: window.innerWidth,
			height: window.innerHeight / 2,
		};

		this.currSymbol = '$';
		this.altSymbol = null;
		this.altValue = null;
		this.amount = null;
		this.onResize = this.onResize.bind(this);
		this.onCurrencyChange = this.onCurrencyChange.bind(this);
	}

	componentDidMount(){
		window.addEventListener('resize', this.onResize);
		window.addEventListener('orientationchange', this.onResize);
	}

	componentWillReceiveProps(newProps){
		const { upCurr, downCurr, rates, amount } = newProps;
		if(Object.keys(rates).length > 0){
			this.currSymbol = symbols[downCurr];
			this.altSymbol = symbols[upCurr];
			this.altValue = getFloats((1 * rates[downCurr]) / rates[upCurr]);
			if(amount){
				this.amount = getFloats((amount * rates[downCurr]) / rates[upCurr]);
			}else{
				this.amount = '';
			}
		}
	}

	componentWillUnmount(){
		window.removeEventListener('resize', this.onResize);
		window.removeEventListener('orientationchange', this.onResize);
	}

	onResize(){
		this.setState({
			width: window.innerWidth,
			height: window.innerHeight / 2,
		});
	}

	onCurrencyChange(base){
		const { setScreenCurrency } = this.props.actions;
		setScreenCurrency(base, 'downScreen');
	}

	render(){
		const { width, height } = this.state;
		return(
			<div className={style.downScreen} style={{ height }}>
				<ScreenSlider value={this.amount}
							  height={height}
							  readOnly={true} display="bottom"
							  dispCurr={this.currSymbol}
							  dispAltCurr={this.altSymbol}
							  dispAltValue={this.altValue}
							  onCurrencyChange={this.onCurrencyChange} />
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(DownScreen);
