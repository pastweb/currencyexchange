import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as curreActions from '$redux/currencies/currActions'; 
import UpScreen from '$layout/upScreen/upScreen';
import DownScreen from '$layout/downScreen/downScreen';

const mapDispatchToProps = (dispatch) => ({
  actions: {
    getCurrentCurrency: bindActionCreators(curreActions.getCurrentCurrency, dispatch),
  },
});

class Layout extends Component {
	constructor(props){
		super(props);
	}

	componentDidMount(){
		const { getCurrentCurrency } = this.props.actions;
		setInterval(() => {
			getCurrentCurrency();
		}, 10000);
		getCurrentCurrency();
	}

	render(){
		return(
			<React.Fragment>
				<UpScreen />
				<DownScreen />
			</React.Fragment>
		);
	}
}

export default connect(null, mapDispatchToProps)(Layout);
