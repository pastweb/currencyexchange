import React from 'react';
import App from './app';
import { shallow } from 'enzyme';

describe('<App />', () => {
	it('renders 1 <App /> component', () => {
		const component = shallow(<App />);
		expect(component).toHaveLenght(1);
	});
});