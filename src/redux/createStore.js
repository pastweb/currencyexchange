import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import initialState from '$redux/initialState';

import makeRootReducer from '$redux/rootReducer';
// import validateMiddleware from '$redux/validate';
// import normaliseMiddleware from '$redux/normalise';

/**
 * @typedef {Object} Redux Store
 * @param {Object} initialState
 * @property {!Function} dispatch actions
 * @property {!Function} getState returns the current state tree
 * @property {Function} replaceReducers replaces the state reducers
 * @property {Function} subscribe attaches an event listener to state changes
 */

export default function createReduxStore(state = initialState) {
  // console.log(state)
  const middlewares = [
    thunkMiddleware,
    // normaliseMiddleware,
    // validateMiddleware,
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__(),
  ].filter(Boolean);

  return compose(...enhancers)(createStore)(makeRootReducer(), state);
}
