import update from 'immutability-helper';
import * as types from './currActionTypes';

const initialState = {
  rates: {},
  upScreen: 'USD',
  downScreen: 'USD',
  amount: '',
};

const actionsObj = {
  [types.SET_CURRENT_CURRENCIES]: function(state, action){
    const { rates } = action;
    return update(state, { rates: { $set: rates }});
  },
  [types.SET_SCREEN_CURRENCY]: function(state, action){
    const { screen, base } = action.data;
    return update(state, { [screen]: { $set: base }});
  },
  [types.SET_AMOUNT]: function(state, action){
    const { amount } = action;
    return update(state, { amount: { $set: amount }});
  },
};


export default (state = initialState, action = {}) => {
  if(actionsObj[action.type]){
    return actionsObj[action.type](state, action);
  }else{
    return state;
  }
};