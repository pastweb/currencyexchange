import api from '$libs/api/api';
import * as types from './currActionTypes';

const APP_ID = '8732d6e362a145c6954319a631e14209';

export function getCurrentCurrency(){
	return dispatch => {
		return api.get(`/latest.json?app_id=${APP_ID}&base=USD`)
		.then(res => {
			const { rates } = res.data;
			dispatch({
			    type: types.SET_CURRENT_CURRENCIES,
			    rates,
			});
		})
		.catch(err => {
			if(err){
				console.error(err);
			}
		});
	};
}

export function setScreenCurrency(base, screen){
	const data = { base, screen };
	return {
		type: types.SET_SCREEN_CURRENCY,
		data,
	}
}

export function setAmount(amount){
	return {
		type: types.SET_AMOUNT,
		amount,
	}
}