import { combineReducers } from 'redux';
import currReducer from '$redux/currencies/currReducer';

export function makeRootReducer(asyncReducers) {
  return combineReducers({
    currencies: currReducer,
    ...asyncReducers,
  });
}

export default makeRootReducer;
