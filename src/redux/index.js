import createStore from '$redux/createStore';
import debug from 'debug';
import makeRootReducer from './rootReducer';

const log = debug('app:state');

/**
 * Don't import store unless you really really need to!
 * If you need to dispatch actions based on external libraries
 * this is a good place to do it.
 */

const store = createStore();
store.asyncReducers = {};

export const injectReducer = (reducerKey, reducer) => {
  if(store.asyncReducers[reducerKey]) return;

  store.asyncReducers[reducerKey] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

/**
 * Export Redux Store
 * You can import store from '$redux' and use store.subscribe or store.dispatch...
 * Assuming the world is about to end, this is a good idea ;)
 */

export default store;
