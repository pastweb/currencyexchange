/**
* Initial Reducer State
*/

const currencies = {
  rates: null,
  upScreen: 'USD',
  downScreen: 'USD',
};

export default {
  currencies,
};
