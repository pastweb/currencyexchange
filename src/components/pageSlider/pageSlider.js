import React, { Component, cloneElement } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './pageSlider.m.scss';

const DOTS_HEIGHT = 35;

class PageSlider extends Component {
	static propTypes = {
		dots: PropTypes.bool,
		dotsColor: PropTypes.string,
		width: PropTypes.number,
		height: PropTypes.number,
		pages: PropTypes.array,
		direction: PropTypes.string,
		slideOut: PropTypes.bool,
		currentPage: PropTypes.number,
	};

	static defaultProps = {
		dots: false,
		dotsColor: 'black',
		width: null,
		height: null,
		pages: [],
		direction: 'left',
		slideOut: false,
		currentPage: null,
	};

	constructor(props){
		super(props);

		const { height, pages, currentPage, width, dots } = this.props;
		this.width = width;

		this.sliderRef = null;
		this.selectedPages = [];
		this.storedPosition = currentPage;
		this.selectPages = this.selectPages.bind(this);
		this.renderDots = this.renderDots.bind(this);
		this.randomKey = this.randomKey.bind(this);

		if(pages[0]){
			const Page = pages[0];
			let slideHeight = height;
			if(dots){
				slideHeight = height - DOTS_HEIGHT;
			}
			this.selectedPages.push(
				<div key={0} className={style.pageSlide} style={{ width: this.width, height: slideHeight }}>
					{cloneElement(Page)}
				</div>
			);
		}
	}

	componentDidMount(){
		const { width } = this.props;
		if(!width){
			setTimeout(() => {
				this.width = this.sliderRef.clientWidth;
				this.forceUpdate();
			}, 50);
		}
	}

	componentWillReceiveProps(newProps){
		const { width, currentPage } = newProps;
		this.selectPages(currentPage, newProps);
		if(width && (width !== this.props.width)){
			this.width = width;
		}
	}

	randomKey(exclude){
		const min = 1, max = 1000;
		let result = Math.floor(Math.random() * (max - min + 1)) + min;
		if(exclude){
			result = Math.floor(Math.random() * (max - min + 1)) + min;
		}
		return result.toString();
	}

	selectPages(newPageNum, props){
		const { height, direction, pages, slideOut, dots } = props;
		const { width } = this;
		let slideHeight = height;
		if(dots){
			slideHeight = height - DOTS_HEIGHT;
		}

		const actions = {
			enter: 'open',
			leave: 'out',
		};
		if(pages[newPageNum-1]){
			let OldPage = pages[this.storedPosition-1];
			let NewPage = pages[newPageNum-1];
			if(newPageNum < this.storedPosition){
				actions.enter = 'out';
				actions.leave = 'in';
				OldPage = pages[newPageNum-1];
				NewPage = pages[this.storedPosition-1];
			}
			const { enter, leave } = actions;
			const key1 = this.randomKey();
			const key2 = this.randomKey(key1);
			this.selectedPages = [
				<div key={key1}
					 className={classNames({
					 				[style.pageSlide]: true,
					 				[style[`slide-${leave}-${direction}`]]: (slideOut && (newPageNum !== this.storedPosition)),
					 			})}
					 style={{ width, height: slideHeight }}>
						{cloneElement(OldPage)}
				</div>,
				<div key={key2}
					 className={classNames({
									[style.pageSlide]: true,
									[style[`slide-${enter}-${direction}`]]: (newPageNum !== this.storedPosition),
								})}
					 style={{ width, height: slideHeight }}>
						{cloneElement(NewPage)}
				</div>
			];
			this.storedPosition = newPageNum;
		}
	}

	renderDots(){
		const { dots, dotsColor, pages, currentPage } = this.props;
		if(dots){
			return <div className={style.dots}>
						{pages.map((page, i) => {
							const dotColor = {
								borderColor : dotsColor,
								backgroundColor: null,
							};
							if(i+1 === currentPage){
								dotColor.backgroundColor = dotsColor;
							}
							return <div className={style.dot} key={i} style={dotColor}/>;
						})}
				   </div>;
		}
	}

	render(){
		const { width, height } = this.props;
		return(
			<div ref={ref => { this.sliderRef = ref; }}
				 className={style.pageSlider}
				 style={{ width, height }} >
				{this.selectedPages}
				{this.renderDots()}
			</div>
		);
	}
}

export default PageSlider;
