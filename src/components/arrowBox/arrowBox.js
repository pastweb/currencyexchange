import React, { Component } from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';

import style from './arrowBox.m.scss';

const attrs = {
	top: {
		color_pos: 'bottom',
		margin_pos: 'left',
		border_color_pos: 'border-bottom-color',
		size_pos: 'margin-left',
	},
	right: {
		color_pos: 'left',
		margin_pos: 'top',
		border_color_pos: 'border-left-color',
		size_pos: 'margin-top',
	},
	bottom: {
		color_pos: 'top',
		margin_pos: 'left',
		border_color_pos: 'border-top-color',
		size_pos: 'margin-left',
	},
	left: {
		color_pos: 'right',
		margin_pos: 'top',
		border_color_pos: 'border-right-color',
		size_pos: 'margin-top',
	},
};

class ArrowBox extends Component {
	static propTypes = {
		arrowPosition: PropTypes.string,
		percent: PropTypes.number,
		arrowSize: PropTypes.number,
		bgColor: PropTypes.string,
		borderColor: PropTypes.string,
		borderWidth: PropTypes.number,
		textColor: PropTypes.string,
		borderRadius: PropTypes.string,
		boxShadow: PropTypes.string,
	};

	static defaultProps = {
		arrowPosition: 'top',
		arrowSize: 10,
		percent: 50,
		bgColor: 'white',
		borderColor: 'black',
		borderWidth: 1,
		textColor: 'black',
	};

	constructor(props){
		super(props);
		
		this.getStyleText = this.getStyleText.bind(this);
	}

	getStyleText(id){
		const {
			arrowPosition,
			percent,
			bgColor,
			arrowSize,
			borderRadius,
			borderColor,
			borderWidth,
			textColor,
			boxShadow,
		} = this.props;

		let after_before = `#arrow-${id}:after, #arrow-${id}:before{
			${attrs[arrowPosition].color_pos}: 100%;
			${attrs[arrowPosition].margin_pos}: ${percent}%}`;
		after_before += `#arrow-${id}:after{
			${attrs[arrowPosition].border_color_pos}: ${bgColor};
			border-width: ${arrowSize}px;
			${attrs[arrowPosition].size_pos}: -${arrowSize}px;
		}`;
		after_before += `#arrow-${id}:before{
			${attrs[arrowPosition].border_color_pos}: ${borderColor};
			border-width: ${arrowSize + borderWidth}px;
			${attrs[arrowPosition].size_pos}: -${arrowSize + borderWidth}px;
		}`;

		let arrow_div = `#arrow-${id}{
			background-color: ${bgColor};
			border: ${borderWidth}px solid ${borderColor};
			color: ${textColor};
		}`;

		if(borderRadius){
			arrow_div += `#arrow-${id}{
				-webkit-border-radius: ${borderRadius};
				-moz-border-radius: ${borderRadius};
				-ms-border-radius: ${borderRadius};
				-o-border-radius: ${borderRadius};
				border-radius: ${borderRadius};
			}`;
		}
		if(boxShadow){
			arrow_div += `#arrow-${id}{
				-webkit-box-shadow: ${boxShadow};
				-moz-box-shadow: ${boxShadow};
				-ms-box-shadow: ${boxShadow};
				-o-box-shadow: ${boxShadow};
				box-shadow: ${boxShadow};
			}`;
		}
		return `${after_before}${arrow_div}`;
	}

	render(){
		const {	width, height, children	} = this.props;
		const id = shortid.generate();
		return(
			<div id={`arrow-${id}`} className={style['arrow-div']}
				 style={{
					width,
					height,
				}}>
				{children}
				<style>
					{this.getStyleText(id)}
				</style>
			</div>
		);
	}
}

export default ArrowBox;
