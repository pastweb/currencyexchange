import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import PageSlider from '$components/pageSlider/pageSlider';
import CurrencyEditor from '$components/currencyEditor/currencyEditor';
import style from './screenSlider.m.scss';

const bases = {
	1: 'USD',
	2: 'GBP',
	3: 'EUR',
};

class ScreenSlider extends Component {
	static propTypes = {
		value: PropTypes.string,
		display: PropTypes.string,
		autoFocus: PropTypes.bool,
		readOnly: PropTypes.bool,
		height: PropTypes.number,
		onAmountChange: PropTypes.func,
		onCurrencyChange: PropTypes.func,
		dispCurr: PropTypes.string,
		dispAltCurr: PropTypes.string,
		dispAltValue: PropTypes.string,
	};
	static defaultProps = {
		value: '',
		display: 'top',
		autoFocus: false,
		readOnly: null,
		height: null,
		onAmountChange: () => {},
		onCurrencyChange: () => {},
		dispCurr: '$',
		dispAltCurr: null,
		dispAltValue: null,
	};
	constructor(props){
		super(props);

		this.state = {
			currentPage: 1,
			direction: 'left',
		};

		const { value } = this.props;
		this.value = value ? value.toString() : '';
		this.onDirectionChange = this.onDirectionChange.bind(this);
		this.changePage = this.changePage.bind(this);
		this.getPages = this.getPages.bind(this);
	}

	componentWillReceiveProps(newProps){
		const { value } = newProps;
		this.value = value ? value.toString() : '';
	}

	onDirectionChange(direction){
		const { currentPage } = this.state;
		if((direction === 'right') && (currentPage !== 1)){
			this.changePage(currentPage - 1, direction);
		}else if((direction === 'left') && (currentPage !== 3)){
			this.changePage(currentPage + 1, direction);
		}
	}

	changePage(newPage, direction){
		this.props.onCurrencyChange(bases[newPage]);
		this.setState({ currentPage: newPage, direction });
	}

	getPages(){
		const { height, autoFocus, readOnly, onAmountChange } = this.props;
		return [
			<CurrencyEditor label="USD" value={this.value} height={height} onChange={onAmountChange} autoFocus={autoFocus} readOnly={readOnly} />,
			<CurrencyEditor label="GBP" value={this.value} height={height} onChange={onAmountChange} readOnly={readOnly}/>,
			<CurrencyEditor label="EUR" value={this.value} height={height} onChange={onAmountChange} readOnly={readOnly} />,
		];
	}

	render(){
		const { height, display, dispCurr, dispAltCurr, dispAltValue } = this.props;
		const { currentPage, direction } = this.state;
		return(
			<div className={style.screenSlider} style={{ height }}>
				<div className={classNames({
									[style.arrow]: true,
									[style.hide]: (currentPage === 1),
								})}
					 style={{ height }}
					 onClick={() => this.onDirectionChange('right')}>
					&lt;
				</div>
				<div className={style.sliderContainer}>
					<div className={`${style.currDisplay} ${style[display]}`}>
						<span className={style.dispCurr}>{dispCurr} 1</span>=<span className={style.dispResult}>{dispAltCurr} {dispAltValue}</span>
					</div>
					<PageSlider height={height - 30}
								pages={this.getPages()}
								direction={direction}
								slideOut={true}
								currentPage={currentPage}
								dots={true}
								dotsColor="white" />

				</div>
				<div className={classNames({
									[style.arrow]: true,
									[style.hide]: (currentPage === 3),
								})}
					 style={{ height }}
					 onClick={() => this.onDirectionChange('left')}>
					&gt;
				</div>
			</div>
		);
	}
}

export default ScreenSlider;
