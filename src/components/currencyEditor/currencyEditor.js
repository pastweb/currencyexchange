import React, { Component } from 'react';
import PropTypes from 'prop-types';
import style from './currencyEditor.m.scss';

class CurrencyEditor extends Component {
	static propTypes = {
		autoFocus: PropTypes.bool,
		readOnly: PropTypes.bool,
		value: PropTypes.string,
		label: PropTypes.string,
		onChange: PropTypes.func,
		height: PropTypes.number,
	};
	static defaultProps = {
		autoFocus: false,
		readOnly: null,
		value: '',
		label: null,
		onChange: () => {},
		height: null,
	};
	constructor(props){
		super(props);

		const { value } = this.props;

		this.value = value;
		this.inputRef = null;
		this.backSpace = false;
		this.onChange = this.onChange.bind(this);
		this.onKeyDown = this.onKeyDown.bind(this);
	}

	componentDidMount(){
		const { autoFocus } = this.props;
		if(autoFocus){
			this.inputRef.focus();
		}
	}

	componentWillReceiveProps(newProps){
		this.value = newProps.value;
	}

	onChange(evt){
		let { value } = evt.currentTarget;
		if(!isNaN(value)){
			if(value.charAt(value.length-1) === '.'){
				if(!this.backSpace){
					value = parseInt(value).toFixed(1);
				}else{
					value = value.substring(0, value.indexOf('.'));
				}
			}else if((value.indexOf('.0') !== -1) && ((value.indexOf('.0')+3) === (value.length -1))){
				value = `${value.substring(0, value.indexOf('.')+1)}${value.substring(value.indexOf('.')+2)}`;
			}else{
				const decimals = value.split('.'); 
				if(decimals[1] && (decimals[1].length === 3)){
					value = `${decimals[0]}.${decimals[1].substring(0, decimals[1].length-1)}`;
				}
			}
			const { onChange } = this.props;
			onChange(value);
			this.value = value;
			this.forceUpdate();
		}else{
			this.forceUpdate();
		}
	}

	onKeyDown(evt){
		const backSpaceCode = 8;
		if(evt.keyCode === backSpaceCode){
			this.backSpace = true;
		}else{
			this.backSpace = false;
		}
	}

	render(){
		const { label, height, readOnly } = this.props;
		return(
			<div className={style.currencyEditor} style={{ height }}>
				<label className={style.label}>{label}</label>
				<input ref={ref => { this.inputRef = ref }}
					   className={style.input}
					   type="text"
					   readOnly={readOnly}
					   value={this.value}
					   onChange={this.onChange}
					   onKeyDown={this.onKeyDown} />
			</div>
		);
	}
}

export default CurrencyEditor;
