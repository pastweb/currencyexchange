import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/**
 * Application Bootstrap.
 */

let domContainerNode = document.getElementById('mount');

ReactDOM.render(<App />,  domContainerNode);

// for hot reloading
if (module.hot) {
    module.hot.accept('./App', () => {

      ReactDOM.unmountComponentAtNode(domContainerNode);
      ReactDOM.render(<App />,  domContainerNode);
    });
}