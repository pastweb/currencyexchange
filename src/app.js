import React from 'react';
import store from '$redux';
import { Provider } from 'react-redux';
import Layout from '$layout';
import './style/app.scss';

const App  = () => <Provider store={store}>
						<Layout />
				   </Provider>;

export default App;
